from odoo import models, fields, api, exceptions

class materials(models.Model):

		_name = 'j2_api.materials'
		_description = 'Model for create materials to sell.'

		material_code = fields.Char('Material Code', required=True)
		material_type = fields.Selection([ ('fabric', 'Fabric'),('jeans', 'Jeans'),('cotton', 'Cotton')],'Material Type', default='fabric')
		supplier_id = fields.Many2one('j2_api.suppliers', required=True, ondelete='restrict', auto_join=True, string='Related Supplier', help='Supplier-related data of the material')
		material_name = fields.Char('Material Name', required=True)
		material_buy_price = fields.Integer('Material Price', required=True)

		@api.constrains('material_buy_price')
		def _check_value(self):
				if self.material_buy_price < 100:
						msg = 'Price can not be less than 100.'
						raise exceptions.ValidationError(msg)

		@api.constrains('material_code', 'material_name')
		def _check_empty(self):
				if self.material_code == '' or self.material_name == '':
						msg = 'Input can not be empty.'
						raise exceptions.ValidationError(msg)


class suppliers(models.Model):

		_name = 'j2_api.suppliers'
		_description = 'Model for create suppliers of the materials.'

		supplier_name = fields.Char('Supplier Name', required=True)
